TOR ansible role
----------------

This role will setup tor and configure it according to variables specified.
It is developed to help with easy setup of :
  - Hidden Services
  - Client use Bridges 
  - Server act as Bridge (obfs4 mode)

Check following example playbooks showing how to use the role.

1. ../playbooks/tor-bridge-server.yml
2. ../playbooks/tor-bridge-client.yml



TESTED ON
  - DEBIAN stretch
*Propably works with other Debian-based distros but is not tested.*

