#!/bin/bash
#
# NAME			reload-revoked.sh
# DESCRIPTION 	Reload OCSP resources
# AUTHOR		@Brucelee
#
{{ cfssl_bin }}/cfssl ocsprefresh \
          -ca {{cfssl_output}}/ca/ca.pem \
          -db-config {{cfssl_output}}/db-driver.json \
          -responder {{cfssl_output}}/ocsp/ocsp.pem \
          -responder-key {{cfssl_output}}/ocsp/ocsp-key.pem

{{ cfssl_bin }}/cfssl ocspdump -db-config {{cfssl_output}}/db-driver.json \
        | tee {{cfssl_output}}/ocspdump.txt
