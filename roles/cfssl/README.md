# CFSSL Ansible Role

This repo contains an ansible role that installs, configures, and run CFSSL Certificate Authority.

Features:
	- run cfssl as a service
	- run cfssl in local mashine without a service



# Create Certificate Authority for Openvpn Server

../../playbooks/cfssl-openvpn-ca.yml