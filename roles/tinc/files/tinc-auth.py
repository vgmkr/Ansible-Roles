#!/usr/bin/env python3
#
# Name: tinc-auth.py
# Author: Brucelee 
# 
# useradd -m -s /bin/bash warrior
# echo "warrior:$(openssl rand -hex 64)" | chpasswd warrior
#
import sys
import re
import os 
import subprocess
import uuid


def authorized_key_line(file):
    conf = open(file, "r").read()
    temp_file = '/tmp/{}'.format(uuid.uuid4().hex)
    # Match public key from tinc configuration
    for key in re.findall('-----BEGIN RSA PUBLIC KEY-----(.*?)-----END RSA PUBLIC KEY-----', conf, re.DOTALL):
        # Create temporary file and write well formated public key into it
        with open(temp_file,'w') as f:
            f.write("-----BEGIN RSA PUBLIC KEY-----\n")
            for line in key.split('\n'):
                if line:
                    f.write("{}\n".format(line))
            f.write("-----END RSA PUBLIC KEY-----")
    # Generate ssh authorized_key compatible output from public key
    rtrn = subprocess.getoutput("ssh-keygen -i -m PEM -f {}".format(temp_file))
    # Remove temporary file if we created
    if os.path.exists(temp_file):
        os.remove(temp_file)
    # Check ssh-keygen gives back the right format and not any error 
    if rtrn.startswith('ssh-rsa'):
        return rtrn
    else:
        return ''

def authorized(user):
    rtrn = []
    path = '/etc/tinc/{}/hosts'.format(user)
    # Check if tinc network exist. User expected to be the network user 'warrior' in
    # our case and should already exist on the running linux box (useradd warrior ...)
    if os.path.exists(path):
        for file in os.listdir(path):
            line = authorized_key_line( '{}/{}'.format(path,file) )
            if line:
                rtrn.append(line)
    return '\n'.join(rtrn)

def main():
    if len(sys.argv) <= 1:
        sys.stderr.write(' ! Please specify username\n')
        sys.exit(1)
    user = sys.argv[1]
    auth_key = authorized(user)
    if auth_key:
        sys.stdout.write( auth_key )
        sys.exit(0)
    sys.exit(1)

if __name__ == '__main__':
    main()