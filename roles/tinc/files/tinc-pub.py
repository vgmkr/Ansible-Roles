#!/usr/bin/env python3
#
# Name: tinc-pub.py
# Author: Brucelee 
# Description: Reads tinc host file load public key and exports it in authorized key format
# Usage: tinc-pub.py /etc/tinc/net/hosts/node
# 
import sys
import re
import os 
import subprocess
import uuid

def generatepub(file):
    conf = open(file, "r").read()
    temp_file = '/tmp/{}'.format(uuid.uuid4().hex)
    # Match public key from tinc configuration
    for key in re.findall('-----BEGIN RSA PUBLIC KEY-----(.*?)-----END RSA PUBLIC KEY-----', conf, re.DOTALL):
        # Create temporary file and write well formated public key into it
        with open(temp_file,'w') as f:
            f.write("-----BEGIN RSA PUBLIC KEY-----\n")
            for line in key.split('\n'):
                if line:
                    f.write("{}\n".format(line))
            f.write("-----END RSA PUBLIC KEY-----")
    # Generate ssh authorized_key compatible output from public key
    rtrn = subprocess.check_output("ssh-keygen -i -m PEM -f {}".format(temp_file).split(' '))
    rtrn = rtrn.decode('utf-8')
    # Remove temporary file if we created
    if os.path.exists(temp_file):
        os.remove(temp_file)
    # Check ssh-keygen gives back the right format and not any error 
    if rtrn.startswith('ssh-rsa'):
        return rtrn
    else:
        return ''

def main():
    if len(sys.argv) <= 1:
        sys.stderr.write(' ! Please specify host file\n')
        sys.exit(1)
    fl = sys.argv[1]
    sys.stdout.write( generatepub(fl) )

if __name__ == '__main__':
    main()