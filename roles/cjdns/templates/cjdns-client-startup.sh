#!/bin/sh
# Name          cjdns-client-startup.sh
# Description   Fixing routes for cjdns client
# Author        Brucelee

{% if cjdns_startup is defined %}
{% for line in cjdns_startup %}
{{line}}
{% endfor %}
{% endif %}

exit 0
