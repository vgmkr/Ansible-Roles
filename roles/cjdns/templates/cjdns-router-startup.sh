#!/bin/sh
# Name          cjdns-router-startup.sh
# Description   Fixing routes for cjdns router (exit-node)
# Author        Brucelee

# Wait for interface to be up and running 
sleep 5

#
#	IPv6
#
{% if cjdns_route_ipv6 %}

# Route default gateway from default interface (eth0)
ip -6 route show \
	| grep '{{cjdns_default_ipv6.gateway}} dev {{cjdns_default_ipv6.iface}}' \
	|| ip -6 route add {{cjdns_default_ipv6.gateway}} dev {{cjdns_default_ipv6.iface}}

# Route default address from default interface (eth0)
ip -6 route show \
	| grep '{{cjdns_default_ipv6.address}} dev {{cjdns_default_ipv6.iface}}' \
	|| ip -6 route add {{cjdns_default_ipv6.address}} dev {{cjdns_default_ipv6.iface}}

# Replace full route from default interface (eth0) to cjdns interface (tun0)
ip -6 route show \
	| grep '{{cjdns_default_ipv6.base}}/{{cjdns_default_ipv6.netmask}}' \
    | xargs -I {} sh -c 'ip -6 route del {}'
ip -6 route add {{cjdns_default_ipv6.base}}/{{cjdns_default_ipv6.netmask}} dev {{cjdns_Interface}}

# Set cjdns address and route to cjdns interface (tun0)
ip -6 addr add {{cjdns_default_ipv6.cjdns}} dev {{cjdns_Interface}} 
ip -6 route add {{cjdns_default_ipv6.cjdns}} dev {{cjdns_Interface}} 

# Add default route via gateway's ipv6"
ip -6 route add default via {{cjdns_default_ipv6.gateway}} \
	|| echo 'ERROR Adding default route via gateway'

# Fix NDP Proxy for cjdns router clients 
{% if cjdns_fix_ndp %}
{% for conn in allowedConnections %}
ip -6 neigh add proxy {{ conn.ip6Address }} dev {{cjdns_default_ipv6.iface}}
{% endfor %}
{% endif %}

{% endif %}


#
#	IPv4
#
{% if cjdns_route_ipv4 %}
ip addr add dev {{cjdns_Interface}} {{ cjdns_default_ipv4.address }}/{{ cjdns_default_ipv4.netmask }}
# ip route add dev tun3 10.66.6.0/24
ip route add {{ cjdns_default_ipv4.base }}0/{{ cjdns_default_ipv4.netmask }} dev {{cjdns_Interface}}
# Setup nat
iptables -t nat -A POSTROUTING -s {{ cjdns_default_ipv4.base }}0/{{ cjdns_default_ipv4.netmask }} -o {{ cjdns_default_ipv4.iface }} -j MASQUERADE
iptables -A FORWARD -i {{ cjdns_default_ipv4.iface }} -o {{cjdns_Interface}} -m state --state RELATED,ESTABLISHED -j ACCEPT
iptables -A FORWARD -i {{cjdns_Interface}} -o {{ cjdns_default_ipv4.iface }} -j ACCEPT
{%endif%}
