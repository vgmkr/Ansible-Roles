# CJDNS


[ entry node ] -- cjdns -- [ exit node ] -- internet


1. Initial installation to get cjdns public keys 

```yaml
- hosts: 
    - cjdsn_entry_nodes
    - cjdsn_exit_nodes
  become: yes
  roles:
    - role: cjdns
      cjdns_client: no
      cjdns_router: no
```

2. Installation of cjdns exit nodes 

Upstream server runs cjdns exit-node where cjdns entry nodes
connect and forward internet connections to.

```yaml
- hosts: 
    - cjdsn_exit_nodes
  become: yes
  
  pre_tasks:

    #  Loop through gateways to get facts
    - name: Gather facts from cjdsn_entry_nodes
      setup:
      delegate_to: "{{ item }}"
      delegate_facts: True
      with_items: "{{ groups['cjdsn_entry_nodes'] }}"
      tags: cjdns

  roles:
    - role: cjdns
      cjdns_router: yes
      
      # Loop through 'gateways' group and load cjdns public keys as list
      cjdns_allowedConnections: "{{ groups['cjdsn_entry_nodes'] | map('extract',hostvars,['ansible_local','cjdns','public','key']) | list  }}"

      # yrd is a command line tool to monitor and interact running cjdns service 
      cjdns_yrd: yes
      cjdns_Interface: tun3
      # Set IPv6 Route
      cjdns_route_ipv6: yes
      cjdns_ip6Prefix: 64
      # Set IPv4 Route
      cjdns_route_ipv4: yes
      cjdns_ip4Prefix: 24
      cjdns_default_ipv4:
        iface: "{{ ansible_default_ipv4.interface }}"
        address: "10.8.6.1"
        base: "10.8.6."
        netmask: "24"
```


3. Installation of cjdns entry nodes 

```yaml
- hosts: 
    - cjdsn_entry_nodes
  become: yes
  pre_tasks:

    - name: Gather facts from cjdsn_exit_nodes server
      setup:
      delegate_to: cjdsn_exit_nodes
      delegate_facts: True
      tags: cjdns

    - name: Create iproute2 table for routing policy
      lineinfile: 
        line: 10 vpn
        path: /etc/iproute2/rt_tables
      tags:
        - cjdns

    - name: Script to set default routing for local network through cjdns exit node
      copy:
        dest: /opt/routing.sh
        content: |
          ip rule  add iif warrior to   {{local_net}} lookup main
          ip rule  add iif warrior from {{local_net}} lookup vpn
          ip route add default via 10.8.6.3 table vpn
      vars:
        local_net: 10.6.66.0/16
      tags:
        - cjdns

  roles:
    - role: cjdns
      cjdns_client: yes
      # Create and use tun3 interface
      cjdns_Interface: tun3
      # # Configure ipv4 network
      # cjdns_ipv4_network: 10.8.6.0
      # cjdns_ipv4_netmask: 255.255.255.0
      # cjdns_ipv4_address: 10.8.6.2
      # cjdns_ipv4_gateway: 10.8.6.1
      cjdns_outgoingConnections: "{{ groups['cjdsn_exit_nodes'] | map('extract',hostvars,['ansible_local','cjdns','public','key']) | list  }}"
      # yrd is a command line tool to monitor and interact running cjdns service 
      cjdns_yrd: yes
      cjdns_startup:
        - iptables -t nat -A POSTROUTING -o tun3  -j MASQUERADE
```