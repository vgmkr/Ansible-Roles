# Obfs proxy installation

Setup server

```yaml
- hosts: 
    - obfs_server
  become: yes
  roles:
    - role: obfs
      obfs_server: true
```

Setup client

```yaml
- hosts: 
    - obfs_client
  become: yes
  roles:
    - role: obfs
      obfs_client: true
      # Ansible server name
      obfs_server_name: obfs_server
```
