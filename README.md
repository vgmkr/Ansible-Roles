# Ansible-Roles

This repo contains ansible roles developed by me over time and some playbooks showing example usage of them.

Roles
-----

1. cfssl

Create Certificate Authority using cfssl to sign/revoke Certiicates. Can be combined with OpenVPN and OCSP.

2. cjdns

Setup CJDNS nodes 

3. dnscrypt

Setup DNSCrypt Proxy to encrypt DNS queries

4. knockd

Port knocking setup for SSH

6. obfs

Obfuscation Proxy Setup

7. tor

Tor Setup supporting Client/Server Bridge Setup.

8. suricata

Suricata IDS installation. Additionally evebox role will install an Dashboard to manage Rules and get some visibility over the alerts/logs.

9. tinc

Tinc VPN setup.
